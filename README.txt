$Id $

File Newest Revision
====================
This module allows for the use of "stable" links to the newest revision of a
file. If node revisions are enabled, and a newer version of a file is uploaded,
the user is automatically redirected to that revision. This is useful for
sending out links to a file outside of your Drupal site, such as through email.

Installation
------------
Simply upload the module to your modules folder and enable it within your site.
No settings or database tables are created by this module.

Use
---
To use this module, upload a file to your site, such as with the Upload module
included with Drupal. To generate a link to the appropriate file, replace
'files', or 'system/files' (if you have private downloads enabled), with
'file-newest'. For example:

http://www.example.com/files/newsletter.pdf

and

http://www.example.com/system/files/newsletter.pdf

would each become:

http://www.example.com/file-newest/newsletter.pdf

If you change between public and private downloads, the appropriate destination
will be determined automatically.

Help and Troubleshooting
------------------------
For help with using this module, please file an issue in the issue queue. If
you find this module useful and wish to add functionality, patches are always
welcome.

Credits
-------
This module was created by Andrew Berry Development
(http://www.abdevelopment.ca/) for Pinchin Environmental
(http://www.pinchin.com/). For paid customization of this module, please
contact Andrew Berry online at http://www.abdevelopment.ca/contact.
